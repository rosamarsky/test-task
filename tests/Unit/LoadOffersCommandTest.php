<?php

namespace TestTask\Tests\Unit;

use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use TestTask\Commands\LoadOffersCommand;
use TestTask\Infrastructure\Repository\InMemoryOfferRepository;
use TestTask\Utils\HttpClient\HttpClient;

class LoadOffersCommandTest extends TestCase
{
    /**
     * @test
     */
    public function test_should_store_data_from_api()
    {
        $repository = new InMemoryOfferRepository;
        $httpClient = new HttpClientStub;

        $loadOffersCommand = new LoadOffersCommand($httpClient, $repository);
        $loadOffersCommand->execute(new TestInput, new TestOutput);

        $this->assertEquals(2, count($repository->all()));
        $this->assertEquals(
            'e7724f3b-d381-48e5-aed3-fcb7c1d81a63',
            $repository->all()[0]->applicationId()
        );
        $this->assertEquals(
            'd54adab2-b4af-48bf-afd8-52438b6e912d',
            $repository->all()[1]->applicationId()
        );
    }

}

class HttpClientStub implements HttpClient
{
    public function request(string $method, string $url, array $params = [], array $headers = []): Response
    {
        return JsonResponse::create([
            [[
                'uid' => 'e7724f3b-d381-48e5-aed3-fcb7c1d81a63',
                'countries' => [
                    'RUS', 'JPN'
                ],
                'payout' => [
                    'amount' => 22.8,
                    'currency' => 'USD'
                ],
                'platform' => 'iOS'
            ]],
            [[
                'id' => 'd54adab2-b4af-48bf-afd8-52438b6e912d',
                'country' => 'UK',
                'points' => 3867,
                'platform' => 'Android'
            ]]
        ]);
    }
}

class TestInput extends Input
{
    public function getFirstArgument(){}
    public function hasParameterOption($values, $onlyParams = false){}
    public function getParameterOption($values, $default = false, $onlyParams = false){}
    public function parse(){}
}

class TestOutput extends Output
{
    public $output = '';

    public function clear()
    {
        $this->output = '';
    }

    protected function doWrite($message, $newline)
    {
        $this->output .= $message.($newline ? "\n" : '');
    }
}