<?php

namespace TestTask\Tests\Unit;

use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use TestTask\Infrastructure\Services\OffersCreator\FirstOfferCreator;
use TestTask\Infrastructure\Services\OffersCreator\OffersCreator;
use TestTask\Infrastructure\Services\OffersCreator\SecondOfferCreator;

class OrderCreatorTest extends TestCase
{
    /**
     * @test
     */
    public function test_should_create_offer_of_first_type()
    {
       $creator = new FirstOfferCreator();
       $offer = $creator->createOffer([
           'uid' => 'd54adab2-b4af-48bf-afd8-52438b6e913d',
           'countries' => ['GBR', 'RUS'],
           'platform' => 'iOS',
           'payout' => [
               'amount' => 12,
               'currency' => 'USD'
           ]
       ]);

       $this->assertEquals((string) $offer->applicationId(), 'd54adab2-b4af-48bf-afd8-52438b6e913d');
       $this->assertEquals($offer->countries(), ['GB', 'RU']);
       $this->assertEquals($offer->platform(), 'iOS');
       $this->assertEquals($offer->payout(), 12);
    }

    /**
     * @test
     */
    public function test_should_create_offer_of_second_type()
    {
       $creator = new SecondOfferCreator();
       $offer = $creator->createOffer([
           'id' => 'd54adab2-b4af-48bf-afd8-52438b6e913d',
           'country' => 'DE',
           'platform' => 'iOS',
           'points' => 1000
       ]);

       $this->assertEquals((string) $offer->applicationId(), 'd54adab2-b4af-48bf-afd8-52438b6e913d');
       $this->assertEquals($offer->countries(), ['DE']);
       $this->assertEquals($offer->platform(), 'iOS');
       $this->assertEquals($offer->payout(), 2);
    }

    /**
     * @test
     */
    public function test_offer_creator_should_create_offers_of_first_type()
    {
        $creator = new OffersCreator(new FirstOfferCreator());
        $offers = $creator->create([
            [
                'uid' => 'd54adab2-b4af-48bf-afd8-52438b6e913d',
                'countries' => ['DEU'],
                'platform' => 'iOS',
                'payout' => [
                    'amount' => 12,
                    'currency' => 'USD'
                ]
            ],
        ]);

        $this->assertEquals((string) $offers[0]->applicationId(), 'd54adab2-b4af-48bf-afd8-52438b6e913d');
        $this->assertEquals($offers[0]->countries(), ['DE']);
        $this->assertEquals($offers[0]->platform(), 'iOS');
        $this->assertEquals($offers[0]->payout(), 12);
    }
}
