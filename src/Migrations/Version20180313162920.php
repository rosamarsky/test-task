<?php
declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180313162920 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TYPE platform AS ENUM ('Android', 'iOS');");

        $this->addSql("CREATE TABLE offers (
                application_id VARCHAR(36),            
                payout_amount DECIMAL(10,2),
                countries VARCHAR(2)[],
                platform platform
            );
        ");

        $this->addSql("CREATE UNIQUE INDEX ON offers (application_id)");
        $this->addSql("CREATE INDEX ON offers (application_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
