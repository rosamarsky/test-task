<?php
declare(strict_types=1);

namespace TestTask\Application\DoctrineTypes;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * PostgreSQL "array" type adapter
 */
class PgArray extends Type
{
    const TYPE_NAME = 'pg_array';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'array';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return explode(',', trim($value, '\{\}'));
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return '{' . implode(',', $value) . '}';
    }

    public function getName()
    {
        return self::TYPE_NAME;
    }
}