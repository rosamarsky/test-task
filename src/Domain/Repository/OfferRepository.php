<?php
declare(strict_types=1);

namespace TestTask\Domain\Repository;

use TestTask\Domain\Entity\Offer;

interface OfferRepository
{
    /**
     * @param Offer $offer
     * @return void
     */
    public function store(Offer $offer): void;
}