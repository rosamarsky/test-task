<?php
declare(strict_types=1);

namespace TestTask\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="offers")
 */
class Offer
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(name="application_id", type="uuid", length=36, unique=true)
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $applicationId;

    /**
     * @var float
     *
     * @ORM\Column(name="payout_amount", type="decimal", precision=10, scale=2)
     */
    private $payoutAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="platform", type="string")
     */
    private $platform;

    /**
     * @var string[]
     *
     * @ORM\Column(name="countries", type="pg_array")
     */
    private $countries = [];

    /**
     * @param UuidInterface $applicationId
     * @param float $payoutAmount
     * @param string $platform
     * @param string[] $countries
     */
    private function __construct(
        UuidInterface $applicationId,
        $payoutAmount,
        $platform,
        array $countries
    ) {
        $this->applicationId = $applicationId;
        $this->payoutAmount = $payoutAmount;
        $this->platform = $platform;
        $this->countries = $countries;
    }

    /**
     * @param UuidInterface $applicationId
     * @param float $payoutAmount
     * @param string $platform
     * @param string[] $countries
     *
     * @return Offer
     */
    public static function create(
        UuidInterface $applicationId,
        $payoutAmount,
        $platform,
        array $countries
    ): Offer
    {
        return new Offer($applicationId, $payoutAmount, $platform, $countries);
    }

    /**
     * @return UuidInterface
     */
    public function applicationId(): UuidInterface
    {
        return $this->applicationId;
    }

    /**
     * @return float
     */
    public function payout()
    {
        return $this->payoutAmount;
    }

    /**
     * @return string
     */
    public function platform(): string
    {
        return $this->platform;
    }

    /**
     * @return string[]
     */
    public function countries(): array
    {
        return $this->countries;
    }
}
