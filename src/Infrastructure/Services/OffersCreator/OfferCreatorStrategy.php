<?php
declare(strict_types=1);

namespace TestTask\Infrastructure\Services\OffersCreator;

use TestTask\Domain\Entity\Offer;

interface OfferCreatorStrategy
{
    public function createOffer(array $data): Offer;
}