<?php
declare(strict_types=1);

namespace TestTask\Infrastructure\Services\OffersCreator;

class OffersCreator
{
    /**
     * @var OfferCreatorStrategy
     */
    private $offerCreatorStrategy;

    /**
     * @param OfferCreatorStrategy $offerCreatorStrategy
     */
    function __construct(OfferCreatorStrategy $offerCreatorStrategy)
    {
        $this->offerCreatorStrategy = $offerCreatorStrategy;
    }

    public function create(array $offers)
    {
        $offerEntities = [];
        foreach ($offers as $offer) {
            $offerEntities[] = $this->offerCreatorStrategy->createOffer($offer);
        }

        return $offerEntities;
    }
}