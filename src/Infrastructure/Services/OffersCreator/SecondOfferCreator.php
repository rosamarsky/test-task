<?php
declare(strict_types=1);

namespace TestTask\Infrastructure\Services\OffersCreator;

use Ramsey\Uuid\Uuid;
use TestTask\Domain\Entity\Offer;

class SecondOfferCreator implements OfferCreatorStrategy
{
    const POINTS_PER_USD = 500;

    /**
     * @param array $data
     * @return Offer
     */
    public function createOffer(array $data): Offer
    {
        return Offer::create(
            Uuid::fromString($data['id']),
            $this->calculatePayout($data['points']),
            $data['platform'],
            [$data['country']]
        );
    }

    /**
     * @param $points
     * @return float
     */
    private function calculatePayout($points): float
    {
        return round($points / self::POINTS_PER_USD, 2);
    }
}