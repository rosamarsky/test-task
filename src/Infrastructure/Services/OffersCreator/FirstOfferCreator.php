<?php
declare(strict_types=1);

namespace TestTask\Infrastructure\Services\OffersCreator;

use League\ISO3166\Exception\OutOfBoundsException;
use League\ISO3166\ISO3166;
use Ramsey\Uuid\Uuid;
use TestTask\Domain\Entity\Offer;

class FirstOfferCreator implements OfferCreatorStrategy
{
    /**
     * @param array $data
     * @return Offer
     */
    public function createOffer(array $data): Offer
    {
        return Offer::create(
            Uuid::fromString($data['uid']),
            (float) $data['payout']['amount'],
            $data['platform'],
            $this->formatCountries($data['countries'])
        );
    }

    /**
     * @param $countries
     * @return array
     */
    private function formatCountries($countries): array
    {
        $formattedCountries = [];
        foreach ($countries as $country) {
            try {
                $formattedCountries[] = (new ISO3166)->alpha3($country)['alpha2'];
            } catch (OutOfBoundsException $e) {
                continue;
            }
        }

        return $formattedCountries;
    }
}