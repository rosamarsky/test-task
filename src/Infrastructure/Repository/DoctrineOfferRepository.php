<?php
declare(strict_types=1);

namespace TestTask\Infrastructure\Repository;

use Doctrine\ORM\EntityManagerInterface;
use TestTask\Domain\Entity\Offer;
use TestTask\Domain\Repository\OfferRepository;

class DoctrineOfferRepository implements OfferRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Offer $offer
     * @return void
     */
    public function store(Offer $offer): void
    {
        $this->entityManager->persist($offer);
        $this->entityManager->flush();
    }
}