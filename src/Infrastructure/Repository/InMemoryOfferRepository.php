<?php
declare(strict_types=1);

namespace TestTask\Infrastructure\Repository;

use TestTask\Domain\Entity\Offer;
use TestTask\Domain\Repository\OfferRepository;

class InMemoryOfferRepository implements OfferRepository
{
    /**
     * @var Offer[]
     */
    private $offers = [];

    /**
     * @return Offer[]
     */
    public function all()
    {
        return $this->offers;
    }

    /**
     * @param Offer $offer
     * @return void
     */
    public function store(Offer $offer): void
    {
        $this->offers[] = $offer;
    }
}