<?php
declare(strict_types=1);

namespace TestTask\Utils\HttpClient;

use Symfony\Component\HttpFoundation\Response;

interface HttpClient
{
    /**
     * @param string $method
     * @param string $url
     * @param array $params
     * @param array $headers
     *
     * @return Response
     */
    public function request(string $method, string $url, array $params = [], array $headers = []): Response;
}