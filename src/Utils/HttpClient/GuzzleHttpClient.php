<?php
declare(strict_types=1);

namespace TestTask\Utils\HttpClient;

use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\Response;

class GuzzleHttpClient implements HttpClient
{
    /**
     * @var ClientInterface
     */
    private $guzzleClient;

    /**
     * @param ClientInterface $guzzleClient
     */
    public function __construct(ClientInterface $guzzleClient)
    {
        $this->guzzleClient = $guzzleClient;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $params
     * @param array $headers
     *
     * @return Response
     */
    public function request(string $method, string $url, array $params = [], array $headers = []): Response
    {
        $response = $this->guzzleClient->request(
            $method,
            $url,
            array_merge(['form_params' => $params], ['headers' => $headers])
        );

        return Response::create(
            $response->getBody(),
            $response->getStatusCode(),
            $response->getHeaders()
        );
    }
}