<?php
declare(strict_types=1);

namespace TestTask\Commands;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TestTask\Domain\Repository\OfferRepository;
use TestTask\Infrastructure\Services\OffersCreator\FirstOfferCreator;
use TestTask\Infrastructure\Services\OffersCreator\OffersCreator;
use TestTask\Infrastructure\Services\OffersCreator\SecondOfferCreator;
use TestTask\Utils\HttpClient\HttpClient;
use Throwable;

class LoadOffersCommand extends Command
{
    const FIRST_CREATION_TYPE = 0;
    const SECOND_CREATION_TYPE = 1;

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @param HttpClient $httpClient
     * @param OfferRepository $offerRepository
     */
    public function __construct(HttpClient $httpClient, OfferRepository $offerRepository)
    {
        parent::__construct();

        $this->httpClient = $httpClient;
        $this->offerRepository = $offerRepository;
    }

    protected function configure()
    {
        $this->setName('app:load:offer')
            ->setDescription('Load an offers from the api.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $data = json_decode($this->httpClient->request('GET', '/api/data')->getContent(), true);

            $offers = [];
            foreach ($data as $type => $items) {
                $creator = $this->getOfferCreator($type);

                if (!$creator) {
                    continue;
                }

                $offers[$type] = $creator->create($items);
            }

            dump($offers);
            $this->storeOffers($offers);
        } catch (UniqueConstraintViolationException $e) {
            $output->writeln('Storage error: Duplicated uid.');
        } catch (Throwable $e) {
            $output->writeln('Critical error: ' . get_class($e));
            $output->writeln('Critical error: ' . $e->getMessage());
        }
    }

    /**
     * @param $type
     * @return OffersCreator|null
     */
    private function getOfferCreator($type)
    {
        switch ($type) {
            case (self::FIRST_CREATION_TYPE):
                return new OffersCreator(new FirstOfferCreator());
            case (self::SECOND_CREATION_TYPE):
                return new OffersCreator(new SecondOfferCreator());
        }

        return null;
    }

    /**
     * @param array $offers
     */
    private function storeOffers(array $offers)
    {
        foreach ($offers as $type => $offerItems) {
            foreach ($offerItems as $offer) {
                $this->offerRepository->store($offer);
            }
        }

    }
}